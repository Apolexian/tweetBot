import unittest
import logging
from src.nlp import create_text


class TestNlp(unittest.TestCase):
    def test_path_creation(self):
        paths_dict = create_text.create_dir("nlp_dir.json")
        self.assertEqual(any(paths_dict), True, msg="Failed to load dir json, check it exists.")

        for path in paths_dict.values():
            logging.info("%s checked for instance string.", path)
            self.assertTrue(type(path) is str, msg="All paths must be of type string, check dir json.")

    def test_text_creation(self):
        test_text = create_text.load_text("data_set/text_data.txt")
        self.assertTrue(test_text.islower(), msg="Loaded text must be lower case.")


if __name__ == '__main__':
    unittest.main()

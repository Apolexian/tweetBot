import logging
import random
from time import sleep
from src.nlp import create_text
import tweepy as tw
import json


# create tweet for bot
def auth():
    """
    :return: api, authenticated with credentials
    """
    with open('creds.json') as creds:
        credentials = json.load(creds)

    authi = tw.auth.OAuthHandler(credentials["consumer_key"], credentials["consumer_secret"])
    authi.set_access_token(credentials["access_token"], credentials["access_secret"])
    api = tw.API(authi, wait_on_rate_limit=True)
    return api


def create_lines():
    """
    :return: the lines from generated text file
    """
    create_text.create_tweet("data_set/text_data.txt")
    with open("tweet_text.txt", "r") as f:
        tweet_lines = f.readlines()
    return tweet_lines


def tweet(tweet_lines):
    """
    :param tweet_lines: the lines to tweet
    :return: n/a
    """
    tweet = ""
    for line in tweet_lines:
        tweet += str(line)
    try:
        logging.warning(f"Tweeting: {tweet}")
        api.update_status(tweet)
        return
    except tw.TweepError as e:
        logging.error(str(e))
        sleep(2)
    logging.warning("Exiting tweet")


def re_tweet(hash_tag):
    """
    :param hash_tag: hash tag to search in order to find things to re-tweet
    :return: n/a
    """
    for tweet in tw.Cursor(api.search, q=hash_tag).items(1):
        try:
            logging.info('\nTweet by: @' + tweet.user.screen_name)
            tweet.retweet()
            logging.info('Retweeted the tweet')
            tweet.favorite()

            if not tweet.user.following:
                tweet.user.follow()
                logging.info(f'Followed the user {tweet.user}')

        except tw.TweepError as e:
            print(e.reason)

        except StopIteration:
            break
    logging.warning("Exiting re_tweet")


if __name__ == '__main__':
    api = auth()
    while True:
        choice = random.randint(0, 100)
        if choice <= 10:
            logging.warning("Tweeting")
            tweet(create_lines())
            sleep(3600)
        else:
            logging.warning("Going into re_tweet")
            re_tweet(random.choice(["#poetry", "#poem", "#shakespear", "#poet", "#poems"]))
            sleep(random.randint(500, 1100))

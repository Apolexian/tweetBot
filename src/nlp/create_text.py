import logging
import os
import numpy as np
from keras import Sequential
from keras.layers import LSTM, Dropout, Dense
from keras.utils import np_utils
from random import randint


def load_text(load_dir):
    """
    :param load_dir: the directory where the text to be loaded is stored
    :return: loaded text in lowercase format
    """
    if os.path.exists(load_dir):
        text_to_load = (open(load_dir).read())
        text_to_load = text_to_load.lower()
        return text_to_load
    else:
        logging.critical(f"{load_dir} could not be found in directory.")
        return None


def create_characters(text_to_convert):
    """
    :param text_to_convert: text to convert to a set of characters
    :return: the set of characters present in the set
    """
    return sorted(list(set(text_to_convert)))


def create_maps(chars):
    """
    :param chars: characters to map
    :return: two maps, number:character and character:number
    """
    n_map = {n: char for n, char in enumerate(chars)}
    char_map = {char: n for n, char in enumerate(chars)}
    return n_map, char_map


def process_data(characters, text_length, sequence_length, process_text, char_to_n, X, Y):
    """
    :param text_length: the length of the text to be processed
    :param sequence_length: the length of the sequence to be produced
    :param process_text: the text we are processing
    :return: X - training matrix, Y - target matrix
    """
    for i in range(text_length - sequence_length):
        sequence = process_text[i:i + sequence_length]
        label = process_text[i + sequence_length]
        X.append([char_to_n[char] for char in sequence])
        Y.append(char_to_n[label])

    x_modified = np.reshape(X, (len(X), sequence_length, 1))
    x_modified = x_modified / float(len(characters))
    y_modified = np_utils.to_categorical(Y)

    return x_modified, y_modified


def create_model(X_mod, Y_mod):
    """
    :return: the model used to train (or loads model if already exists
    """
    model = Sequential()
    model.add(LSTM(700, input_shape=(X_mod.shape[1], X_mod.shape[2]), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(700))
    model.add(Dropout(0.2))

    model.add(Dense(Y_mod.shape[1], activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam')
    if os.path.exists("models/generation_model.h5"):
        model.load_weights("models/generation_model.h5")
        logging.info("Model Loaded.")
    else:
        model.fit(X_mod, Y_mod, epochs=30, batch_size=200)
        model.save_weights("models/generation_model.h5")

    return model


# generating characters
def generate_text(string_mapping, characters, model, n_to_char, full_string):
    """
    :param string_mapping: the mapping of characters in the string
    :return: generated text that will be used as a tweet
    """
    for i in range(200):
        x = np.reshape(string_mapping, (1, len(string_mapping), 1))
        x = x / float(len(characters))

        pred_index = np.argmax(model.predict(x, verbose=0))
        full_string.append(n_to_char[pred_index])

        string_mapping.append(pred_index)
        string_mapping = string_mapping[1:len(string_mapping)]

    txt = ""
    for char in full_string:
        txt = txt + char

    with open("tweet_text.txt", "w") as f:
        f.write(txt)
        logging.info(txt)

    return txt


def create_tweet(text_dir):
    X = []
    Y = []
    text = load_text(text_dir)
    characters = create_characters(text)
    length = len(text)
    seq_length = 50
    n_to_char, char_to_n = create_maps(create_characters(text))
    logging.info("Mappings created.")
    X_mod, Y_mod = process_data(characters, length, seq_length, text, char_to_n, X, Y)
    logging.info("Data processed.")
    model = create_model(X_mod, Y_mod)
    logging.info("Model created/loaded successfully.")

    try:
        string_mapped = X[randint(0, 2000)]
        full_string = [n_to_char[value] for value in string_mapped]
        text_returned = generate_text(string_mapped, characters, model, n_to_char, full_string)
        logging.info(f"Text created for tweet: {text_returned}.")
    except IndexError as e:
        logging.info(str(e))
        pass


if __name__ == '__main__':
    create_tweet()

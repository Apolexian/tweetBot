FROM python:3.7

RUN pip install numpy tensorflow keras

WORKDIR ./src
COPY ./src /src/

ENTRYPOINT ["python"]
CMD ["nlp/create_text.py"]
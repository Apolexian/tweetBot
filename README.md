TweetBot
------
Personal project the end point of which would be to create
a twitter-bot that acts as a normal user and see how many followers it can get!
The bot will **not** spam or break any terms or conditions set by Twitter.

Part 1 of the project -NLP
--------------------------
The first part of this project is the text generation.
The bot needs something to tweet, and I wanted it to be more or less human-like.
The NLP model used here is quite hefty and takes a while to train (personally I used an Azure VM).
After the model is done training its saved and can be loaded, so there is no need to re-train it, unless a 
new data-set needs to be used for text generation.

| Tech          | Version       | 
| ------------- |:-------------:|
| Python        | 3.6.8         |
| Docker        | 19.03.x       | 

Libraries that are not included in standard-libs: numpy, tensorflow,keras.

Part 2 of the project - Twitter API
-----------------------------------
In order to interact with the twitter account I will be using the Twitter API and the tweepy library.
The bot is set to have a 10% chance to generate a new sonnet and tweet it, after which it waits for an hour (to not spam).
Otherwise (90% chance) that the bot will find a tweet with the hash-tag "poetry" and re-tweet it (also follow the person
who tweeted it if it doesn't follow them already). After which it will wait 15 mins (again,to not spam).

Libraries that are not included in standard-libs: tweepy.

